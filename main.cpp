//
//  hw311_single_linked_list.cpp
//
//
//  Created by Benjamin J. Benson on 10/1/15.
//
//


/*
 * Name: Put your name(s) here
 */
#ifndef GVSU_CS263_SINGLE_LINKED_LIST
#define GVSU_CS263_SINGLE_LINKED_LIST

#include <iostream>
#include <stdio.h>
using namespace std;

/* The following single_linked_list class shall be implemented WITHOUT
 * using any of the C++ data structures (std::vector, std::list, std::map,
 * ...)
 *
 * This assignment is primarily a reinforcement of writing recursive
 * functions to manipulate a recursive data structure.
 */

template <typename E>
class single_linked_list {
private:
    /* a struct is a class whose members are all public */
    struct Node {
        E data;   /* type E from the template */
        Node* next;
    };
    
    Node *head;
public:
    /* the following public functions are NOT RECURSIVE, but they will
     * invoke the recursive counterpart */
    
    /* Complete each of the following public function */
    
    /* constructor */
    single_linked_list() {
        head = nullptr;
    }
    
    /* destructor */
    ~single_linked_list() {
        cout << "We're deleting"<<endl;
        _delete(head);
        head = nullptr;
    }
    
    unsigned long size() const {
        return _size(head);
    }
    
    void print () {
        _print(head);
    }
    
    bool is_contained (const E& x){
        return _is_contained (head, x);
    }
    
    void addItem (const E& x) {
        addElement(head, x);
    }
    
    void remove (const E& x) {
        _remove(head, x);
    }
    
    void print_reverse () {
        _backwards(head);
    }
    
private:
    
    /* the following private functions are RECURSIVE */
    unsigned long _size (Node *from) const {
        if (from != nullptr) {
            int size_after_me = _size(from->next);
            return 1 + size_after_me;
        }
        else
            return 0;
    }
    
    void print_backwards(Node* &head) {
        if(head == NULL){
            return;
        }else{
            print_backwards(head->next);
            cout <<"Node at the end: \n"<<to_string(head->data);
        }
    }
    
    void _backwards(Node* a){
        int i = _size(a);
        for(i; i>= 0; --i){
            int j = 1;
            a = head;
            while(j < i){
                if(a->next == nullptr){
                    break;
                }else{
                    a = a->next;
                }
                j++;
            }
            cout << a->data<<"\n";
        }
        
    }
    
    void _print (Node* &curr) const {
        if ( curr == nullptr) {
            return;
        }
        cout << curr->data<<endl;
        _print(curr->next);
    }
    
    /**Is contained works in the linked list.**/
    bool _is_contained (Node* &curr, const E &val) const {
        //base case of the recursion
        if(curr == nullptr){
            return false;
        }
        //assuming that there
        else if(curr->data == val){
            return true;
        }else{
            return _is_contained(curr->next, val);
        }
        return false;
    }
    
    /**Method to recursively add an element into a single linked list.**/
    void addElement(Node* &curr, const E &item){
        if(curr != nullptr){
            addElement(curr->next, item);
            
        }else{
            //The next node should be the next one
            Node *temp = new Node{item, nullptr};
            curr = temp;
            return;
        }
    }
    
//    /**Helper for the removal of a node**/
//    void help_remove(Node* &curr, const E &item){
//        //do nothing if current is nullptr
//        if(curr != nullptr){
//            
//            //there's only one item.
//            if(head == curr && curr->next == nullptr && curr->data == item){
//                head = nullptr;
//                return;
//            }
//            
//            if(curr == head && curr->data == item){
//                head = head->next;
//                return;
//            }
//
//            if(curr->next != nullptr && curr->next->data == item){
//                Node* &temp = curr->next;
//                curr->next = curr->next->next;
//                delete temp;
//                return;
//                //found it change the ptr and delete the node.
//            }
//            //keep recurring until you find the node you're looking for.
//            else{
//                help_remove(curr->next, item);
//            }
//        }
////        cout << "you returned without deleting anyting"<<endl;
//        return;
//    }
    
bool _remove(Node* &curr, int val){
    
    if (curr == NULL){
        return false;
    }
    
    if (curr->data == val){
        delete curr;
        return true;
    }
    
    if (curr->next && curr->next->data == val){
        Node* del = curr->next;
        curr->next = del->next;
        delete del;
        return true;
    }
    return _remove(curr->next, val);
}
    
void _delete(Node* &head){
        
    if (head == nullptr) {
        return;
    }else {
        _delete(head->next);
        delete head;
    }
}
    
    /* TODO Add more private recursive functions here */
    
};
#endif


int main(){
    single_linked_list<int> test;
    for(int i = 0; i<10; ++i){
        test.addItem((i+1)*10);
    }
    
//    test.print();
//    
//    if(test.is_contained(40)){
//        cout << "The number: "<<to_string(40)<<" is contained."<<endl;
//    }else{
//        cout << "The number: "<<to_string(40)<<" is NOT contained."<<endl;
//    }
//    //This is not going to work but w/e
//    test.remove(40);
//    if(test.is_contained(40)){
//        cout << "The number: "<<to_string(40)<<" is contained."<<endl;
//    }else{
//        cout << "The number: "<<to_string(40)<<" is NOT contained."<<endl;
//    }
//    
//    test.remove(10);
//    if(test.is_contained(10)){
//        cout << "The number: "<<to_string(10)<<" is contained."<<endl;
//    }else{
//        cout << "The number: "<<to_string(10)<<" is NOT contained."<<endl;
//    }
//
    test.remove(100);
    if(test.is_contained(100)){
        cout << "The number: "<<to_string(100)<<" is contained."<<endl;
    }else{
        cout << "The number: "<<to_string(100)<<" is NOT contained."<<endl;
    }
    
    test.remove(40);
    if(test.is_contained(40)){
        cout << "The number: "<<to_string(40)<<" is contained."<<endl;
    }else{
        cout << "The number: "<<to_string(40)<<" is NOT contained."<<endl;
    }

    cout << "The rest of the list looks like this..."<<endl;
    test.print();
    
    cout << "The size of the list looks like this..."<<to_string(test.size())<<endl;
    
    cout << "Testing print backwards:  \n\n";
    test.print_reverse();
    
    cout << "Testing print forwards: \n\n";
    test.print();
}


